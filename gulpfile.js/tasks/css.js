var config       = require('../config')
if(!config.tasks.css) return

var handleErrors = require('../lib/handleErrors')
var autoprefixer = require('autoprefixer')
var browserSync  = require('browser-sync')
var cssnano      = require('cssnano')
var gulp         = require('gulp')
var gulpif       = require('gulp-if')
var postcss      = require('gulp-postcss')
var sass         = require('gulp-sass')
var sourcemaps   = require('gulp-sourcemaps')
var path         = require('path')

var paths = {
  src: path.join(config.root.src, config.tasks.css.src, '/**/*.{' + config.tasks.css.extensions + '}'),
  dest: path.join(config.root.dest, config.tasks.css.dest)
}

var devProcessors = [
  autoprefixer(config.tasks.css.autoprefixer)
];

var prodProcessors = [
  autoprefixer(config.tasks.css.autoprefixer),
  cssnano({autoprefixer: false})
];

var cssTask = function () {
  return gulp.src(paths.src)
    .pipe(gulpif(!global.production, sourcemaps.init()))
    .pipe(sass(config.tasks.css.sass))
      .on('error', handleErrors)
    .pipe(gulpif(global.production, postcss(prodProcessors)))
    .pipe(gulpif(!global.production, postcss(devProcessors)))
    .pipe(gulpif(!global.production, sourcemaps.write()))
    .pipe(gulp.dest(paths.dest))
    .pipe(browserSync.stream())
}

gulp.task('css', cssTask)
module.exports = cssTask
