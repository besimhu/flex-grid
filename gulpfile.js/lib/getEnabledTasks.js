var config  = require('../config')
var compact = require('lodash/compact')

// Grouped by what can run in parallel
var codeTasks = ['markup', 'css']

module.exports = function(env) {

  function matchFilter(task) {
    if(config.tasks[task]) {
      return task
    }
  }

  function exists(value) {
    return !!value
  }

  return {
    codeTasks: compact(codeTasks.map(matchFilter).filter(exists))
  }
}
